# Utiliser une version d'Alpine compatible avec ARMv6
FROM arm32v6/alpine:latest
#FROM alpine:latest

# Installer Python et pip
RUN apk add --no-cache python3 py3-pip g++ python3-dev

# Mettre à jour pip
# See https://archlinuxarm.org/forum/viewtopic.php?p=64598#p64598
RUN pip3 install --upgrade --break-system-packages pip && \
    CFLAGS="-fcommon" pip3 install --break-system-packages RPi.GPIO && \
    apk del g++ python3-dev

# Définir le répertoire de travail
WORKDIR /app

# Copier les fichiers requirements.txt et les scripts Python dans le répertoire de travail
COPY requirements.txt ./

# Installer les dépendances Python à partir de requirements.txt
RUN pip3 install --no-cache-dir --break-system-packages -r requirements.txt

# Copier le reste des fichiers nécessaires pour l'application
COPY *.py .

# Commande pour exécuter le script principal lors du démarrage du conteneur
CMD ["python3", "-u", "main.py"]
