#!/bin/bash

cd /srv/iot-dht22

# Define the remote and branch you want to check for updates
remote="origin"
branch="main"

# Fetch latest changes from the remote repository
git fetch $remote

# Check if the local branch is behind the remote branch
if git diff --quiet HEAD $remote/$branch; then
  echo "No changes detected."
else
  echo "Changes detected. Pulling latest changes..."
  git pull $remote $branch
  
  # Add the command or script you want to run below
  echo "Restart dht-sensor..."

  systemctl daemon-reload

  /usr/local/bin/pip3 install -r requirements.txt --break-system-packages

  service dht-sensor restart

fi
