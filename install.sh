#!/bin/bash

apt-get install -y python3-dev python3-openssl python3-pip python3-sysv-ipc libgpiod2 

python3 -m pip install --upgrade pip setuptools wheel --break-system-packages

systemctl disable bluetooth
systemctl stop bluetooth
systemctl disable hciuart
systemctl stop hciuart

pip3 install -r requirements.txt --break-system-packages

ln -s /srv/iot-dht22/dht-sensor.service /etc/systemd/system/dht-sensor.service

systemctl daemon-reload

systemctl enable dht-sensor

systemctl start dht-sensor

git config pull.ff only 
