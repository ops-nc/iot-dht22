#!/usr/bin/env python3
import os
import csv
import time
import socket
import datetime
import requests
import threading
import Adafruit_DHT
from s3_move_files import move_files_to_s3
from influx_send_datas import send_datas_to_influx
requests.packages.urllib3.disable_warnings()

version = "v0.1"

# Configuration - Variables
LOCAL_FOLDER = 'datas/'
DELAY = os.environ.get('DELAY')
BUCKET_NAME = os.environ.get('BUCKET_NAME')
ACCESS_KEY = os.environ.get('S3_ACCESS_KEY')
SECRET_KEY = os.environ.get('S3_SECRET_KEY')
S3_ENDPOINT_URL = os.environ.get('S3_ENDPOINT_URL')
#SENSOR_NAME = os.environ.get('SENSOR_NAME', 'default_sensor')
SENSOR_NAME = socket.gethostname()
INFLUX_URL = os.environ.get('INFLUX_URL')
DEBUG = 1

# Sensor config
SENSOR = Adafruit_DHT.DHT22
PIN = 4

if not os.path.exists(LOCAL_FOLDER):
    os.makedirs(LOCAL_FOLDER)

while True:
    filename = os.path.join(LOCAL_FOLDER, f"{datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')}-{SENSOR_NAME}.csv")
    try:
        humidity, temperature = Adafruit_DHT.read_retry(SENSOR, PIN)
        if humidity is not None and temperature is not None:
            with open(filename, mode='w', newline='') as file:
                writer = csv.writer(file, delimiter=';')
                writer.writerow(['Date', 'Température', 'Humidité', 'Sensor', 'Version'])
                now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                writer.writerow([now, f"{temperature:.1f}", f"{humidity:.1f}", SENSOR_NAME, version])
            print(f"{now} - {SENSOR_NAME} - Température: {temperature:.1f}°C, Humidité: {humidity:.1f}%")
        else:
            raise ValueError("Lecture du capteur échouée...")
    except Exception as e:
        now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        error_message = f"{now} - Erreur lors de la lecture du capteur: {e}"
        print(error_message)

    try:
        thread = threading.Thread(target=move_files_to_s3, args=(BUCKET_NAME, LOCAL_FOLDER, ACCESS_KEY, SECRET_KEY, S3_ENDPOINT_URL))
        thread.start()
    except Exception as e:
        now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        error_message = f"{now} - Erreur de l'envoie vers S3 : {e}"
        print(error_message)

    if INFLUX_URL and humidity is not None and temperature is not None:
        try:
            humidityround = round(humidity, 1)
            temperatureround = round(temperature, 1)
            thread = threading.Thread(target=send_datas_to_influx, args=(SENSOR_NAME, INFLUX_URL, temperatureround, humidityround))
            thread.start()
        except Exception as e:
            now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            error_message = f"{now} - Erreur de l'envoie vers Influx : {e}"
            print(error_message)

    time.sleep(int(DELAY))

