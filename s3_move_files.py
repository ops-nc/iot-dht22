import os
import boto3
from botocore.exceptions import NoCredentialsError

def move_files_to_s3(bucket_name, local_folder, ACCESS_KEY, SECRET_KEY, S3_ENDPOINT_URL):
    s3_client = boto3.client(
        's3',
        endpoint_url=S3_ENDPOINT_URL,
        aws_access_key_id=ACCESS_KEY,
        aws_secret_access_key=SECRET_KEY, verify=False)

    for filename in os.listdir(local_folder):
        local_path = os.path.join(local_folder, filename)
        if os.path.isfile(local_path):
            try:
                s3_client.upload_file(local_path, bucket_name, filename)
                os.remove(local_path)
                print(f"Fichier {filename} uploadé avec succès.")
            except NoCredentialsError:
                print("Erreur d'authentification S3. Vérifiez vos identifiants.")
                break
            except Exception as e:
                print(f"Erreur lors du téléversement de {filename}: {e}")
                break
