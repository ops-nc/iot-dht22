from flask import Flask, render_template
import pandas as pd
import plotly.express as px
import plotly.io as pio
import boto3
from io import StringIO
import os

app = Flask(__name__)

# AWS S3 Configuration using environment variables
BUCKET_NAME = 'iot-temp-datas'
ACCESS_KEY = os.environ.get('S3_ACCESS_KEY')
SECRET_KEY = os.environ.get('S3_SECRET_KEY')
S3_ENDPOINT_URL = os.environ.get('S3_ENDPOINT_URL')

# Initialize Boto3 S3 client
s3_client = boto3.client(
    's3',
    aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY,
    endpoint_url=S3_ENDPOINT_URL,
    verify=False
)

def load_data():
    """Load and aggregate data from all CSV files in the S3 bucket using pagination, handling empty or malformed files."""
    paginator = s3_client.get_paginator('list_objects_v2')
    pages = paginator.paginate(Bucket=BUCKET_NAME)

    df_list = []
    for page in pages:
        if 'Contents' in page:
            for file in page['Contents']:
                obj = s3_client.get_object(Bucket=BUCKET_NAME, Key=file['Key'])
                csv_data = obj['Body'].read().decode('utf-8')
                # Check if the CSV data is not empty and contains the expected delimiter
                if csv_data.strip() and ';' in csv_data:
                    try:
                        df = pd.read_csv(StringIO(csv_data), delimiter=';')
                        df_list.append(df)
                    except pd.errors.EmptyDataError:
                        print(f"Skipping empty or malformed file: {file['Key']}")
                else:
                    print(f"Skipping empty or non-CSV file: {file['Key']}")

    if df_list:
        full_data = pd.concat(df_list, ignore_index=True)
    else:
        full_data = pd.DataFrame()  # Return an empty DataFrame if no valid files were found
    return full_data


@app.route('/')
def index():
    df = load_data()

    # Plot temperature graph
    fig_temp = px.line(df, x='Date', y='Température', title='Temperature over Time')
    graph_temp = pio.to_html(fig_temp, full_html=False)

    # Plot humidity graph
    fig_humidity = px.line(df, x='Date', y='Humidité', title='Humidity over Time')
    graph_humidity = pio.to_html(fig_humidity, full_html=False)

    return render_template('index.html', graph_temp=graph_temp, graph_humidity=graph_humidity)

if __name__ == '__main__':
    app.run(debug=True)


