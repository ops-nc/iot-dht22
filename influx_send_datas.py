import requests

def send_datas_to_influx(SENSOR_NAME, INFLUX_URL, temperatureround, humidityround):
    data = f'environment,sensor={SENSOR_NAME} temperature={temperatureround},humidity={humidityround}'
    response = requests.post(INFLUX_URL, data=data)

    if response.status_code != 204:
        print("Failed to write data to influx...")
        print(response.status_code, response.text)
